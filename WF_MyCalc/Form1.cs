﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_MyCalc
{
    public partial class FormMain : Form
    {
        private enum Operation
        {
            Addition,
            Subtraction,
            Multiplication,
            Division
        }

        private Operation operation;
        private bool wasCalculated = true;
        private bool labelHistoryIsFilled = false;
        private bool repeatC = false;
        private double a, b, c, tmp;
        private string templabelHistoryText1 = "";
        private string templabelHistoryText2 = "";
        private string lastOperator = "";
        private bool buttonEqualWasPushed = false;

        private void Calculate()
        {
            switch (operation)
            {
                case Operation.Addition:
                    c = double.Parse(textBox.Text);
                    b = a + c;
                    textBox.Text = b.ToString();
                    break;
                case Operation.Subtraction:
                    c = double.Parse(textBox.Text);
                    b = a - c;
                    textBox.Text = b.ToString();
                    break;
                case Operation.Multiplication:
                    c = double.Parse(textBox.Text);
                    b = a * c;
                    textBox.Text = b.ToString();
                    break;
                case Operation.Division:
                    c = double.Parse(textBox.Text);
                    if (c == 0.0)
                    {
                        MessageBox.Show("Деление на ноль!",
                            "Сообщение",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information,
                            MessageBoxDefaultButton.Button1,
                            MessageBoxOptions.DefaultDesktopOnly);
                        textBox.Text = "0";
                    }
                    else
                    {
                        b = a / c;
                        textBox.Text = b.ToString();
                    }
                    break;
            }
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void DeleteZeroInTextBox()
        {
            if (textBox.Text == "0")
            {
                textBox.Text = "";
            }
        }

        private void FillLabelHistory()
        {
            templabelHistoryText1 += templabelHistoryText2 + lastOperator;
            labelHistory.Text = templabelHistoryText1;
            wasCalculated = false;
        }

        private void FillHistory()
        {
            if (labelHistoryIsFilled == false)
            {
                FillLabelHistory();
                labelHistoryIsFilled = true;
            }
        }

        private void ResetNumber()
        {
            if (buttonEqualWasPushed == true)
            {
                textBox.Clear();
                labelHistory.Text = "";
                templabelHistoryText1 = "";
                templabelHistoryText2 = "";
            }
            buttonEqualWasPushed = false;
        }

        #region buttons 0 - 9

        private void button0_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            DeleteZeroInTextBox();

            FillHistory();
            ResetNumber();

            textBox.Text += "9";
        }

        #endregion

        #region button .

        private void buttonPoint_Click(object sender, EventArgs e)
        {
            ResetNumber();

            if (textBox.Text.IndexOf(',') == -1)
            {
                FillHistory();
                textBox.Text += ",";
            }
        }

        #endregion

        #region button C

        private void buttonC_Click(object sender, EventArgs e)
        {
            repeatC = true; 
            textBox.Text = "0";
            labelHistory.Text = "";
            templabelHistoryText1 = "";
            templabelHistoryText2 = "";
            lastOperator = "";
            wasCalculated = true;
            labelHistoryIsFilled = true;
        }

        #endregion

        #region buttons +,-,*,/

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            if (buttonEqualWasPushed == true)
            {
                c = a;
                ResetNumber();
                buttonEqualWasPushed = false;
            }

            if (wasCalculated == false && textBox.Text != "")
            {
                Calculate();
            }

            if (textBox.Text != "")
            {
                a = double.Parse(textBox.Text);
                textBox.Clear();
            }

            if (repeatC == true)
            {
                c = a;
                repeatC = false;
            }

            templabelHistoryText2 = c.ToString();
            
            operation = Operation.Addition;
            lastOperator = "+";

            labelHistory.Text = templabelHistoryText1 + templabelHistoryText2 + lastOperator;
            labelHistoryIsFilled = false;

            wasCalculated = true;
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            if (buttonEqualWasPushed == true)
            {
                c = a;
                ResetNumber();
                buttonEqualWasPushed = false;
            }

            if (wasCalculated == false && textBox.Text != "")
            {
                Calculate();
            }

            if (textBox.Text != "")
            {
                a = double.Parse(textBox.Text);
                textBox.Clear();
            }

            if (repeatC == true)
            {
                c = a;
                repeatC = false;
            }

            templabelHistoryText2 = c.ToString();

            operation = Operation.Subtraction;
            lastOperator = "-";

            labelHistory.Text = templabelHistoryText1 + templabelHistoryText2 + lastOperator;
            labelHistoryIsFilled = false;

            wasCalculated = true;
        }

        private void buttonMult_Click(object sender, EventArgs e)
        {
            if (buttonEqualWasPushed == true)
            {
                c = a;
                ResetNumber();
                buttonEqualWasPushed = false;
            }

            if (wasCalculated == false && textBox.Text != "")
            {
                Calculate();
            }

            if (textBox.Text != "")
            {
                a = double.Parse(textBox.Text);
                textBox.Clear();
            }

            if (repeatC == true)
            {
                c = a;
                repeatC = false;
            }

            templabelHistoryText2 = c.ToString();

            operation = Operation.Multiplication;
            lastOperator = "*";

            labelHistory.Text = templabelHistoryText1 + templabelHistoryText2 + lastOperator;
            labelHistoryIsFilled = false;

            wasCalculated = true;
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            if (buttonEqualWasPushed == true)
            {
                c = a;
                ResetNumber();
                buttonEqualWasPushed = false;
            }

            if (wasCalculated == false && textBox.Text != "")
            {
                Calculate();
            }

            if (textBox.Text != "")
            {
                a = double.Parse(textBox.Text);
                textBox.Clear();
            }

            if (repeatC == true)
            {
                c = a;
                repeatC = false;
            }

            templabelHistoryText2 = c.ToString();

            operation = Operation.Division;
            lastOperator = "/";

            labelHistory.Text = templabelHistoryText1 + templabelHistoryText2 + lastOperator;
            labelHistoryIsFilled = false;

            wasCalculated = true;
        }

        #endregion

        private void buttonEqual_Click(object sender, EventArgs e)
        {
            if (textBox.Text != "")
            {
                Calculate();
            }
            else
            {
                textBox.Text = "0";
            }

            labelHistory.Text += c.ToString() + "=";

            wasCalculated = true;
            labelHistoryIsFilled = true;
            buttonEqualWasPushed = true;
            a = b;
        }

        private void buttonBackspace_Click(object sender, EventArgs e)
        {
            int length = textBox.Text.Length - 1;
            string text = textBox.Text;

            textBox.Clear();

            for (int i = 0; i < length; i++)
            {
                textBox.Text = textBox.Text + text[i];
            }

            if (textBox.Text.Length == 0)
            {
                buttonC.PerformClick();
            }
        }

        private void buttonNegative_Click(object sender, EventArgs e)
        {
            if (textBox.Text != "")
            {
                a = double.Parse(textBox.Text) * (-1.0);
                textBox.Text = a.ToString();
            }
        }

        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            if (textBox.Text != "")
            {
                a = Math.Sqrt(double.Parse(textBox.Text));
                textBox.Text = a.ToString();
            }
        }

        private void buttonProcent_Click(object sender, EventArgs e)
        {
        }

        private void buttonPow_Click(object sender, EventArgs e)
        {
            if (textBox.Text != "")
            {
                a = Math.Pow(double.Parse(textBox.Text), 2);
                textBox.Text = a.ToString();
            }
        }

    }
}
